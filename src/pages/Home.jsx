import { useState } from 'react'
// import { FaChevronDown, FaChevronUp } from 'react-icons/fa6'
import booksJson from '../data/books.json'

export const Home = () => {
  const [isDragging, setIsDragging] = useState(false)
  const [numberOfPages, setNumberOfPages] = useState(5)
  const [selectedBooks, setSelectedBooks] = useState([])
  // const [isOpen, setIsOpen] = useState(false)

  const handleChangeDrag = (event) => {
    event.preventDefault()
    setIsDragging(!isDragging)
  }
  const targetClassName = `m-14 p-10 bg-slate-100 border-dashed border-slate-300 border-2 min-h-60 ${
    isDragging ? 'border-indigo-600' : ''
  }`

  const handleDragStart = (event) => {
    event.dataTransfer.setData('book-id', event.target.id)
  }

  const handleDrop = (e) => {
    e.preventDefault()
    const id = e.dataTransfer.getData('book-id')
    const item = booksJson.library[id]
    if (item) {
      setSelectedBooks([...selectedBooks, item])
    }
  }

  const onDragOver = (e) => {
    e.preventDefault()
    e.stopPropagation()
  }

  return (
    <div>
      <div className="w-2/4 p-24">
        <h5 className="mb-2 text-4xl font-bold tracking-tight text-gray-900">
          8 libros disponibles
        </h5>

        <div className="flex flex-row items-center justify-between">
          <div>
            <label
              htmlFor="range-pages"
              className="block mb-2 text-sm font-medium text-gray-900"
            >
              Filtrar por número de páginas
            </label>
            <input
              id="range-pages"
              type="range"
              min="0"
              max="10"
              value={numberOfPages}
              className="w-full h-2 bg-gray-200 rounded-lg appearance-none cursor-pointer"
              onChange={(e) => setNumberOfPages(e.target.value)}
            >
              {}
            </input>
          </div>

          <div>
            <label
              htmlFor="dropdown-category"
              className="block mb-2 text-sm font-medium text-gray-90"
            >
              Filtrar por categoría
            </label>
            <div className="relative w-full lg:max-w-sm">
              <select className="w-full p-2.5 text-gray-500 bg-white border rounded-md shadow-sm outline-none appearance-none focus:border-indigo-600">
                <option>ReactJS Dropdown</option>
                <option>Laravel 9 with React</option>
                <option>React with Tailwind CSS</option>
                <option>React With Headless UI</option>
              </select>
            </div>
          </div>
        </div>
      </div>

      <div className="grid grid-cols-2 gap-1">
        <div className="w-full items-center justify-center p-24">
          <div className=" grid grid-cols-2 md:grid-cols-4 gap-2">
            {booksJson.library.map((bookItem, index) => (
              <img
                key={index}
                id={index}
                className="max-w-full rounded-lg cursor-grab"
                draggable="true"
                src={bookItem.book.cover}
                alt=""
                onDragStart={(event) => handleDragStart(event)}
              ></img>
            ))}
          </div>
        </div>
        <div
          className={targetClassName}
          onDragEnter={handleChangeDrag}
          onDragLeave={handleChangeDrag}
          onDragOver={onDragOver}
          onDrop={handleDrop}
        >
          <div className="grid grid-cols-2 md:grid-cols-4 gap-2 ">
            {selectedBooks.map((bookItem, index) => (
              <img
                key={index}
                id={index}
                className="max-w-full rounded-lg"
                src={bookItem.book.cover}
                alt=""
              ></img>
            ))}
          </div>
        </div>
      </div>
    </div>
  )
}
